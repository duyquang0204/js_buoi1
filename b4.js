/**
 * 1.Input
 * Nhập chiều dài
 * Nhập chiều rộng
 * 
 * 2.Các bước thực hiên
 * Chu vi chữ nhật = (chiều dài+chiều rộng)*2
 * Diện tích chữ nhật = chiều dai*chiều rộng
 * 
 * 3.Output
 * Chu vi chữ nhật
 * Diện tích chữ nhật
 */

var chieuDai = 7;
var chieuRong = 3;
var chuViChuNhat = null;
var dienTichChuNhat = null;

chuViChuNhat = (chieuDai + chieuRong)*2;
dienTichChuNhat = chieuDai*chieuRong;

console.log("Chu vi chữ nhật",chuViChuNhat);
console.log("Diện tích chữ nhật",dienTichChuNhat);